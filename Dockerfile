FROM python:3.9.6-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /app

ENV HOME=/app
ENV APP_HOME=/app/web
WORKDIR $APP_HOME
RUN mkdir $APP_HOME/staticfiles

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

RUN pip install --upgrade pip

COPY ./requirements.txt .

RUN pip wheel --no-cache-dir --no-deps --wheel-dir $APP_HOME/wheels -r requirements.txt
RUN pip install --no-cache $APP_HOME/wheels/*
RUN addgroup -S app && adduser -S app -G app

RUN apk add libpq


COPY ./entrypoint.sh .
RUN sed -i 's/\r$//g'  $APP_HOME/entrypoint.sh
RUN chmod +x  $APP_HOME/entrypoint.sh

COPY . $APP_HOME

RUN chown -R app:app $APP_HOME

USER app

ENTRYPOINT ["/app/web/entrypoint.sh"]
